# Memulai project

![Flo](./images/scrum.png)

## Persiapan Team
Ketika memulai project, Product Owner(PO) membentuk team yang terdiri dari:

- Backend developer (BE)
- Frontend developer (FE)
- UI/UX Designer (UI/UX)
- System Architecture (SA)
- Scrum master (SM)

Setelah team terbentuk, PO mengumpulkan SM, SA, UI Designer kemudian membahas kesuluruhan Business Flow.
Setelah berdiskusi Output yang dihasilkan berupa Product Backlog Item dan user story untuk keseluruhan aplikasi.

Kemudian SM bersama Development Team, berdiskusi untuk mengestimasi waktu pengerjaan dan memprioritaskan feature yang akan dikerjkan.
Untuk estimasi waktu, DT yang menentukan tapi tetap dengan persetujuan dari PO.

Setelah semua disetujui, maka SM akan memasukan semua PBI ke jira lengkap dengan story point dan meyiapkan PBI untuk scrum yang akan dilaksanakan.

Untuk SA dan UI/UX bisa berjalan tanpa harus mengikuti ketentuan pada sprint, jadi mereka bisa menyelesaikan task mereka sekaligus pada awal sprint

Untuk Frontend dan Backend Developer diwajibkan mengikuti alur pada sprint

Agar integrasi antara FE dan BE tidak saling ketergantungan, SA diwajibkan membuat [JSON mock](http://api.mock.phillip.eigen.co.id/) untuk project terlebih dahulu. JSON mock dapat dibuat bertahap sesuai dengan prioritas dari PBI.

## Daily Development

- Setelah semua PBI disiapkan oleh SM, kemudian DT membuat task sendiri berdasarkan PBI yang sudah disiapkan oleh SM lengkap dengan `estimasi` waktunya.
- Jika DT mendapatkan kendala masalah business flow, maka DT dapat berdiskusi dengan SM
- DT wajib memiliki task pada `today progress` setiap harinya
- Setiap hari sekitar jam 10 - jam 11, dilaksanakan `daily stand up` oleh SM untuk membahas `apa yang telah dikerjakan`, `apa yang akan dikerjakan`, dan `apa yang menjadi kendala`
- DT wajib melakukan `push` pada branch setiap harinya sebelum pulang dan melakukan pull request, jika memang ada yang masih belum bisa dipush, DT bisa berdiskusi dahulu dengan SM
- Code yang telah dipush oleh DT dilakukan pengecekan oleh tester, jika code lolos maka akan dilanjut dengan review
- Code yang telah dipush oleh DT akan direview dahulu oleh reviewer sebelum di merge dengan branch `development` atau `master`
- Untuk penjelasan melakukan git akan dibahas di `Code Review`


## Sprint restropektif

Setiap 2 minggu sekali setelah berakhir sprint, akan dilakukan sprint restropektif untuk membahas dan mereview semua aktivitas pada sprint sebelumnya, dan akan dijadikan acuan untuk sprint berikutnya