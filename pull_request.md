# Code Review

![Flo](./images/flow.png)

Code review sangat penting agar code yang ditulis DT tetap satu buku dengan guide yang ditetapka. `pull request` digunakan untuk meriview kode pada `branch` sebelum mencapai `master`. Code review juga merupakan salah satu bagian yang paling sulit dan memakan waktu dari proses pengembangan perangkat lunak, seringkali membutuhkan anggota tim yang berpengalaman untuk meluangkan waktu membaca, berpikir, mengevaluasi, dan menanggapi implementasi fitur atau sistem baru.

## Ketentuan pull request
- Sebelum memulai development DT membuat branch baru dengan kententuan
	- `feature/` untuk membuat fitur atau modul baru
	- `bugfix/` untuk memelakukan perbaikan bug pada development
	- `hotfix/` untuk melakukan perbaikan pada release
	- `release/` ketika siap meluncurkan aplikasi
- Judul pada PR harus berisi tag `[Fix]`, `[Feature]`, `[Refactor]`, `[Release]`, `[Hotfix]`
Contoh:
```
[Feature] Implemetasi API untuk membuat data product
```
- Deskripsi PR harus berisi JIRA ticket, Penjelasan PR, dan mentag reviewer
Contoh:
```
Mengimplementasi fungsi untuk mencari code product menggunakan code product atau code variant
PW-101
```
- Satu ticker JIRA berlaku untuk satu PR
- Selalu check checkbox untuk menutup PR jiga sudah di merge

# Code ketika membuat feature

## Pull branch `master` atau `development`
```
git pull origin master
```

## Membuat branch baru
```
git branch feature/news
git checkout feature/news
```

## Setelah melakukan perubahan pull branch `master` atau `development` kembali
```
git pull origin master
```

## Push perubahan ke branch yang dibuat
```
git push origin feature/news
```

## Membuat pull request