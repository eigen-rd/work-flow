# Memulai development feature

![Flo](./images/flow.png)

## Pull branch `master` atau `development`
```
git pull origin master
```

## Membuat branch baru
```
git branch feature/news
git checkout feature/news
```

## Setelah melakukan perubahan pull branch `master` atau `development` kembali
```
git pull origin master
```

## Push perubahan ke branch yang dibuat
```
git push origin feature/news
```

## Membuat pull request